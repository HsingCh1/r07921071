
let sorting = (array) => {
	var temp;
	for(var i = 0; i < array.length; i++){
		for(var j = i+1; j < array.length; j++){
			if(array[j] < array[i]){
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}	
    return array;
}

let compare = (a, b) => {	
	return (a["PM2.5"] - b["PM2.5"]) > 0 ? 1:-1
}

let average = (nums) => {
	var sum = 0;
	for(var i = 0; i < nums.length; i++){
		sum += nums[i];
	}
	return Math.round((sum/nums.length + Number.EPSILON) * 100) / 100;
}


module.exports = {
    sorting,
    compare,
    average
}
